Marketplace Service
===================

API to get products for the marketplace

Endpoints
---------

`/api/products` Get all available products

`/api/bestrate` Get the best available rate
